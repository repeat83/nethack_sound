-- player https://stsaz.github.io/fmedia/#download

-- current directory (where is file locate)
PWD = string.gsub(debug.getinfo(1).source, "^@(.+\\)[^\\]+$", "%1")

PLAYER = "\""..PWD.."fmedia/fmedia.exe\" --notui" --background"
-- "--globcmd.pipe-name=nethack_action --globcmd=listen" -- опции для управления музыкой, переключения, остановки..ю
PLAYER_PRM = ""
-- sound from file "TomeNET 4.7.3 for Windows complete installer (sound and music pack included)" at https://www.tomenet.eu/downloads.php
SOUND_PATH = "\""..PWD.."xoomer.virgilio.it_pid/" --"TomeNET_music+sound/sound/"
print("SOUND_PATH "..SOUND_PATH)
CMD_START = "start /b \"\""
--..PLAYER.." "..SOUND_PATH

local delay_in_MS_between_sounds = 200

-- sount at startup
--for i = 1,10 do
--	os.execute(CMD_PLAY.."combat/hit_weapon/whip2.ogg")
--	sleep(1)
--end

-- index 1 - sound. can be table
-- index 2 - (optional) volume. 0-125
actions = {}


actions["This door is locked."] = {"lock.ogg"}
actions["The door resists!"] = {"door1.ogg"}
actions["The door opens."] = {"door2.ogg"}
actions["You hear a door open."] = {"door2.ogg",15} --50%
actions["The door closes."] = {"door3.ogg"}
actions[".*it crashes open.*"] = {"crack.ogg"}
actions["The door crashes open.*"] = {"crack.ogg"}
actions[".*The door splinters!.*"] = {"crack.ogg"}
actions[".*door .* shatters.*"] = {"crack.ogg"}
actions["WHAMMM!!!"] = {"whammm.ogg"}

actions["You carefully open the .*"] = {"creak.ogg",15} --50%
actions["Hmmm, it seems to be locked."] = {"lock.ogg",25} --75%
actions[".*slams open.*"] = {"slams.ogg"}
actions[".*break open the lock.*"] = {"crack.ogg"}
actions["THUD!"] = {"thud.ogg"}

actions["Klunk!.*"] = {"metal.ogg"}

actions[".*move the boulder.*"] = {"boulder.ogg"}
actions[".*You finish eating.*"] = {"munch.ogg"}
actions[".*lack of food.*"] = {"moan.ogg"}
actions[".*needs food, badly!"] = {"moan.ogg",25} --75%
actions["You are beginning to feel hungry."] = {"moan.ogg",15} --50%
actions["You are beginning to feel weak."] = {"moan.ogg",25} --75%
actions[".*puts you to sleep.*"] = {"yawn.ogg"}
actions[".*put to sleep by.*"] = {"yawn.ogg"}
actions[".*Yak - dog food!.*"] = {"yak.ogg"}
actions[".*vomit.*"] = {"vomit.ogg"}
actions["Blecch!.*"] = {"blecch.ogg"}
actions[".*Maud.*"] = {"maud.ogg"}

actions[".*crumbles and turns to dust.*"] = {"crumble.ogg"}
actions["You begin to memorize the runes.*"] = {"book.ogg"}

actions["You swing your .* through thin air."] = {"air.ogg"}
actions["You kick at empty space."] = {"air.ogg"}
actions["You attack thin air."] = {"air.ogg"}
actions["Snap!"] = {"snap.ogg"}

actions[".*You succeed in cutting away.*"] = {"rocks.ogg"}
actions[".*You make an opening in the.*"] = {"rockl.ogg"}
actions["You start digging downward.*"] = {"rockl.ogg"}
actions[".*This wall is too hard.*"] = {"vault.ogg"}
actions["The boulder fills a pit."] = {"fills.ogg"}

actions[".*glows silver.*"] = {"harp.ogg"}
actions[".*glows blue.*"] = {"harp.ogg"}
actions[".*surrounded by a shimmering light.*"] = {"shimmer.ogg"}
actions[".*Magical energies course through .*"] = {"shimmer.ogg"}
actions[".*glows black.*"] = {"black.ogg"}
actions[".*You feel like someone is helping.*"] = {"helping.ogg"}
actions[".*coalesces in your mind.*"] = {"helping.ogg"}

actions[".*door .* shatters.*"] = {"crack.ogg"}
actions[".*shatter.*"] = {"shatter.ogg"}
actions[".*breaks into shards.*"] = {"shatter.ogg"}
actions[".*applause.*"] = {"applause.ogg"}

actions["You bought .*"] = {"register.ogg"}
actions[".* gold pieces."] = {"coins.ogg"}
actions["You hear someone counting money."] = {"coinsfall.ogg",15} --50%
actions["You hear someone counting gold coins."] = {"coinsfall.ogg",15} --50%
actions["Your purse feels lighter."] = {"coins.ogg",13} --25%
actions[".*punished for your misbehavior.*"] = {"chain.ogg"}

actions[".*magic missile.*hits.*"] = {"missile.ogg"}
actions[".*lightning bolt.*hits.*"] = {"lightning.ogg"}
actions[".*magic missile.*bounces.*"] = {"missile.ogg"}
actions[".*lightning bolt.*bounces.*"] = {"lightning.ogg"}
actions[".*The .* bounces!.*"] = {"bounces.ogg"}
actions[".*blinded by the flash.*"] = {"camera.ogg"}

actions[".*You hit the.*"] = {"hit1.ogg"}
actions[".*You smite the.*"] = {"hit1.ogg"}
actions[".*You kick the.*"] = {"hit1.ogg"}
actions[".*You destroy the.*"] = {"hit2.ogg"}
actions[".*You kill the.*"] = {"hit2.ogg"}
actions[".*The .* hits.*"] = {"scream1.ogg"}
actions[".*The .* bites.*"] = {"growl.ogg"}
actions[".*bits the.*"] = {"hit1.ogg"}
actions[".*bites the.*"] = {"growl.ogg"}
actions[".*Ouch!.*"] = {"ouch.ogg"}
actions[".*You get zapped.*"] = {"spark.ogg"}
actions[".*You miss the.*"] = {"swing.ogg"}
actions[".*The .* misses.*"] = {"swing.ogg"}
actions[".*You are splashed by.*"] = {"slop.ogg"}
--The goblin throws a crude dagger!  You are hit by a crude dagger.

actions[".*You die..."] = {"evil.ogg"}

actions[".*gush of water hits.*"] = {"gush.ogg"}
actions["You hear the splashing of a naiad."] = {"naiad.ogg"}
actions["You hear bubbling water."] = {"bubbs.ogg"}
actions["You hear a strange wind."] = {"wind.ogg"}
actions[".*You hear a slow drip.*"] = {"drip.ogg"}
actions[".*a monster behind the boulder.*"] = {"bison.ogg"}
actions["Water gushes forth.*"] = {"water.ogg"}
actions["You fall into water!.*"] = {"dive.ogg"}
actions[".*almost hit by.*"] = {"almost.ogg"}
actions[".*You are hit by a .*dart.*"] = {"dart.ogg"}
actions[".*You are hit by an arrow.*"] = {"arrow.ogg"}
actions["You feel self-knowledgeable..."] = {"magic1.ogg"}
actions["You .* the presence of.*"] = {"magic1.ogg"}
actions[".*nose tingles and you smell.*"] = {"sniff.ogg"}
actions[".*moving.*faster.*"] = {"faster.ogg"}
actions["You speed up."] = {"faster.ogg"}

actions["You hear.*noises in the distance."] = {"skrap.ogg"}
actions[".*howling of the CwnAnnwn..."] = {"howling.ogg"}
actions[".*cool draught refreshes you.*"] = {"draught.ogg"}

actions["You feel very attracted to.*"] = {"crowd-groan.ogg"}

actions[".*You make a lot of noise.*"] = {"noise.ogg"}
actions["You hear a gurgling noise."] = {"gurgling.ogg"}
actions[".*gushes up from the drain.*"] = {"gurgling.ogg"}
actions["You hear .* cash register."] = {"register.ogg"}
actions["You hear the footsteps.*"] = {"footsteps.ogg"}
actions["You hear a crashing sound.*"] = {"crack.ogg"}
actions[".* suddenly disappears!"] = {"beam.ogg"}
actions[".*teleporter trap.*"] = {"beam.ogg"}
actions[".*explosion.*"] = {"explosion.ogg"}
actions[".*bear trap closes.*"] = {"beartrap.ogg"}
actions[".*KABOOM!!.*"] = {"kaboom.ogg"}
actions["Yecch! You've been creamed.*"] = {"cream.ogg"}
actions[".*opening the tin.*"] = {"tin.ogg"}

actions[".*more confident in your .* skills.*"] = {"circus.ogg"}
actions["You are now more skilled in .*"] = {"circus.ogg"}

actions[".*ghost touches you.*"] = {"ghost1.ogg"}
actions[".*You destroy .* ghost!.*"] = {"ghost2.ogg"}
actions[".*You miss.*ghost.*"] = {"swing.ogg"}
actions[".*ghost misses.*"] = {"swing.ogg"}

actions[".*Welcome to experience level.*"] = {"firework.ogg"}
actions[".*Welcome to.*"] = {"shopdoor.ogg"}
actions[".*Welcome again to.*"] = {"shopdoor.ogg"}
actions[".*Delphi.*"] = {"fanfare.ogg"}

actions[".*monkey stole.*"] = {"monkey.ogg"}


function mysplit (inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        -- замена спец блока на точки, чтоб потом разрезать сообщения
    	seq_r = "\x1b%(B\x1b%[0;1m"
        inputstr = inputstr:gsub(seq_r,"\x01")
        inputstr = inputstr:gsub("%.%s*",".\x01")
        inputstr = inputstr:gsub("!%s*","!\x01")
        inputstr = inputstr:gsub("?%s*","?\x01")		

        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end


function action_sound(msg)
-- Read the content of the trace buffer
	local value0 = msg
--print("action_sound run")
-- найти всё кроме разделителей . ! ?
-- это будут предложения которые надо анализировать на предмет текста
-- дополнительно режем по спец кодам -- \(B\[0;1m
	messages = mysplit(value0,"\x01")

	for _,msg in pairs(messages) do
--print("msg:\n"..msg)
		-- search patter in message for playing sound
		for pattern,action in pairs(actions) do
			if string.match(msg,".*"..pattern..".*") then
				sound = action[1]
				PLAYER_PRM = action[2] and "--volume="..action[2].." " or ""

				if type(sound) == "table" then
					--math.randomseed(os.clock()*100000000000)		    
					local k = math.random(#sound)
					sound = sound[k]						
				end
				local cmd = CMD_START.." "..PLAYER.." "..PLAYER_PRM.." "..SOUND_PATH..sound.."\""
				os.execute(cmd)
				print(msg,cmd)
				sleep(delay_in_MS_between_sounds) 
			end
		end
	end
--print("action_sound end")
end

function action_sound_old(msg)
-- Read the content of the trace buffer
	local value0 = msg

-- найти всё кроме разделителей . ! ?
-- это будут предложения которые надо анализировать на предмет текста
-- дополнительно режем по спец кодам -- \(B\[0;1m
	for msg in string.gmatch(value0, "([^%.^!^%?]+)") do
	
		-- search patter in message for playing sound
		for pattern,sound in pairs(actions) do
			if string.match(msg,".*"..pattern..".*") then

				if type(sound) == "table" then
					--math.randomseed(os.clock()*100000000000)		    
					local k = math.random(#sound)
					sound = sound[k]						
				end
				os.execute(CMD_PLAY..sound)
				print(msg,sound)
				sleep(delay_in_MS_between_sounds) 
			end
		end
	end

end