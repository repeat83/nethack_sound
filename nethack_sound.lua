-- чтение файла лога Putty
-- парсинг лога, отделение входящих сообщений от прочих
-- очистка входящих сообщений лога от мусора (восстановление исходного сообщения)
-- анализ сообщения на наличие текста
--- настройка: анализировать не весь текст, а только тот, что печатается в качестве информационных сообщений
-- проигрывание звука в зависимости от текста

--local puttylog = string.gsub(debug.getinfo(1).source, "^@(.+\\)[^\\]+$", "%1")
--puttylog = puttylog.."..\\putty.log"

local puttylog = os.getenv("PWD")-- or io.popen("cd"):read()
puttylog = puttylog.."..\\putty.log"
print(puttylog)

local log = io.open(puttylog,'r')

log:seek("end") -- go to end of file after start script

function sleep(ms)
  local ntime = os.clock() + ms/1000
  repeat until os.clock() > ntime
end

require "action_sound"

action_sound("This door opens.")
sleep(100)
action_sound("You hear a door open")

function parse_All_session_output(buf)
-- берем только ту строку, которая похожа на строку сообщения
--\[5d\(B\[0;1m -- тут "5" это на какой строке писать текст
-- с этих символов начинается текст. 3-й по порядку это строка на которой текст пишется. текст подсвечен белым!! может быть важно.
-- в 3-й позикии может быть строка от 10 и более, тогда выглядит так:
-- \[13d\(B\[0;1m

--1b 5b 3x 64 1b 28 42 1b 5b 30 3b 31 6d
--\  [  5  d  \  (  B  \  [  0  ;  1  m
	--local start_pos = buf:find("\x1b\x5b..\x64\x1b\x28\x42\x1b\x5b\x30\x3b\x31\x6d")

-- лучше искать подвеченный текст, который начинается с такого:
-- \(B\[0;1m
	local seq = "\x1b\x28\x42\x1b\x5b\x30\x3b\x31\x6d"
	--local seq = "\x1b(B\x1b[0;1m"
	local start_pos = buf:find(seq,1,true) -- plain text found without regexp
--io.write("start_pos ")
--print(start_pos)

	if (start_pos == nil) or (start_pos > 6) then return ""; end -- если не нашли или сиквенс начала текста больше чем 6 позиция, то это скорее всего не текст и не обрабатываем такие случаи

	start_pos = start_pos and start_pos+seq:len() -- сдвигаем начала "курсор" на начало непосредственно текста.
--io.write("seq:len() ")
--print(buf:find("\x1b",start_pos,true))

	if buf:find("\x1b",start_pos,true) < 4+start_pos then print("short white message"); return ""; end -- если ESC символ находится менее чем на 4й позиции от начала текста, значит тут текста нет, либо он очень короткий. Не обрабатываем такие строки.
	
	buf = start_pos and buf:sub(start_pos) or "" -- если не нашли начало текста, вернем пустышку
	return buf
end

local buf
local txt = ""
while (true) do

--TODO: интересно, строки он разбивает по 0d 0a? надо бы чтоб так делал
	buf = log:read("L")

	if buf ~= nil then
	-- режем буфер по строкам с разделителем 0x0D
		for buf_0 in buf:gmatch("([^\x0d]*)") do
			txt = parse_All_session_output(buf_0)
		end
	end
	
	-- если передача сообщения завершена - показываем полученное сообщение
	if txt:len() > 0 then
		--print(txt)
		action_sound(txt)
		txt = ""
	end
end
